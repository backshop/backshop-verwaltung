package database;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.springframework.data.annotation.Id;

@Entity
public class Person {
	
	//pid, nachname, vorname, geschlecht, gebdatum, 
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pid;
	private String nachname;
	private String vorname;
	private boolean geschlecht;
	private Date gebdatum;

	
	
}
