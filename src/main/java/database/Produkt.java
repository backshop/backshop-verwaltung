package database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.springframework.data.annotation.Id;

@Entity
public class Produkt {

	//anzlager, preis, bezeichnung, pid
	//werte aus datenbank
	
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 private int pid;
	 private String bezeichnung;
	 private int anzlager;
	 private double preis;
	
	 public Produkt(String bezeichnung, int anzlager, double preis) {
	        this.bezeichnung = bezeichnung;
	        this.anzlager = anzlager;
	        this.preis = preis;
	    }

	@Override
	public String toString() {
		return "Produkt [pid=" + pid + ", bezeichnung=" + bezeichnung
				+ ", anzlager=" + anzlager + ", preis=" + preis + "]";
	}

	   
	 
	 
	 
}
