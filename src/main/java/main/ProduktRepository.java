package main;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import database.Produkt;

public interface ProduktRepository extends CrudRepository<Produkt, Long> {

	List<Produkt> findByProdukts(String beichnung);
	
    //List<Produkt> findByLastName(String beichnung);
}